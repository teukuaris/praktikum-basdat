Berikut adalah demonstrasi dan penjelasan penggunaan Transaction dalam basis data Shopee:

1. Pembelian Produk: Ketika pengguna melakukan pembelian produk di Shopee, transaksi dibuat dalam basis data untuk mencatat detail pembelian, seperti identitas pengguna, produk yang dibeli, jumlah, harga, dan metode pembayaran. Transaksi ini memastikan bahwa data pembelian tersimpan dengan aman dan konsisten.

2. Update Stok Produk: Setelah pembelian produk, basis data perlu diperbarui untuk mengurangi stok produk yang tersedia. Dalam Transaction, operasi pengurangan stok dan pembaruan informasi produk dilakukan secara bersamaan. Jika ada beberapa pengguna yang melakukan pembelian produk yang sama secara bersamaan, penguncian (locking) diterapkan untuk mencegah konflik dan memastikan bahwa stok produk yang tersedia diperbarui dengan benar.

3. Konfirmasi Pembayaran: Setelah pengguna melakukan pembayaran, transaksi digunakan untuk mencatat konfirmasi pembayaran, seperti informasi metode pembayaran, jumlah yang dibayarkan, dan waktu pembayaran. Data ini penting untuk memastikan bahwa pembayaran telah dilakukan dengan sukses dan pesanan dapat diproses.

4. Lacak Pengiriman: Transaksi juga digunakan dalam basis data Shopee untuk melacak status pengiriman produk. Ketika pengguna melacak pengiriman, transaksi mengupdate informasi pengiriman, seperti nomor resi, status pengiriman, dan perkiraan waktu pengiriman. Pengguna dapat mengakses informasi ini melalui fitur pelacakan pengiriman di aplikasi Shopee.

5. Konfirmasi Penerimaan dan Penilaian: Setelah pengguna menerima pesanan, transaksi digunakan untuk mencatat konfirmasi penerimaan dan penilaian. Pengguna dapat memberikan penilaian atau ulasan tentang produk yang dibeli. Transaksi ini memastikan bahwa penilaian dan ulasan yang diberikan terhubung dengan pesanan yang tepat.

6. Penyelesaian Sengketa: Jika terjadi masalah dalam transaksi, seperti barang tidak sesuai atau pesanan tidak diterima, transaksi dalam basis data Shopee dapat digunakan untuk memulai penyelesaian sengketa. Data transaksi penting dalam menyelesaikan masalah dan memberikan solusi yang adil kepada pengguna.

Melalui penggunaan Transaction dalam basis data Shopee, informasi penting terkait pembelian, pengiriman, penilaian, dan penyelesaian sengketa diatur dan terjamin konsistensinya. Transaksi memastikan bahwa operasi-operasi yang saling terkait dilakukan dengan benar dan menghindari kesalahan atau anomali dalam basis data.
